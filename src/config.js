export const routes = {
    login: "/",
    startScreen: "/home",
    userProfile: "/profile",
    register: "/register",
    searchfriendsList: "/searchfriends",
    friendsList: "/friendslist",
    admin: '/admin',
    confirmation: "/confirm",
    emailForgotPassword: "/forgotpassword",
    confirmationView: '/confirmationview',
    emailForgotUsername: "/forgotusername",
    newPassword: "/forgot",
    confirmProfileDataUpdate: '/confirmprofileupdate',
    bugs: '/bugs',
    singlePlayer: '/singleplayer'
}

export const authenticationToken = "authenticationToken";

export const regex = {
    email: new RegExp('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'),
    /* must contain at least 1 Uppercase, 1 lowercase, 1 number, 1 special character and
     * 8 characters in total
    */
    password: new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')
}

export const validationTypes = {
    PATTERN: 'pattern',
    REQUIRED: 'required',
    EQUALITY: 'notEquivalent'
}

 let backEndIp = window.location.href
 backEndIp = backEndIp.substring(backEndIp.indexOf('//') + 2, backEndIp.length)
 // backEndIp = backEndIp.substring(0, backEndIp.indexOf(':'));

 export const apiUrls = {
     register: `http://${backEndIp}backend/api/user/register`,
     login: `http://${backEndIp}backend/api/user/login`,
     authToken: `http://${backEndIp}backend/api/user/authtoken`,
     createUser: `http://${backEndIp}backend/api/user/validate`,
     update: `http://${backEndIp}backend/api/user/update`,
     currentUser: `http://${backEndIp}backend/api/user/profile`,
     mailForgotPassword: `http://${backEndIp}backend/api/user/forgotpassword`,
     deleteOwnProfile: `http://${backEndIp}backend/api/user/delete`,
     mailForgotUsername: `http://${backEndIp}backend/api/user/forgotusername`,
     adminGetAllUsers: `http://${backEndIp}backend/api/user/`,
     adminFindUsers: `http://${backEndIp}backend/api/user/search`,
     profileImageUpload: `http://${backEndIp}backend/api/user/uploadpicture`,
     profileImageDownload: `http://${backEndIp}backend/api/user/downloadpicture`,
     adminBlacklistUser: `http://${backEndIp}backend/api/user/blacklist`,
     adminWhitelistUser: `http://${backEndIp}backend/api/user/blacklist/undo`,
     bugs: `http://${backEndIp}backend/api/bug`,
     game: `http://${backEndIp}backend/api/game/`
 }
