import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import PlayingFieldPlayer from '../shared/components/playingFieldPlayer';
import PlayingFieldComputer from '../shared/components/playingFieldComputer';

const BattleshipSinglePlayerShipsLoaded = (props) => {
    return (
        <Fragment>
            <div className="row">
                <PlayingFieldPlayer playerName="You" ships={props.ships} />
                <PlayingFieldComputer playerName="Computer" gameId={props.gameId} />
            </div>
        </Fragment>
    );
}

BattleshipSinglePlayerShipsLoaded.propTypes = {
    ships: PropTypes.array,
    gameId: PropTypes.number
}

export default BattleshipSinglePlayerShipsLoaded;
